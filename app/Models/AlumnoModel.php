<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of AlumnoModel
 *
 * @author jose
 */
class AlumnoModel extends Model{ //Ahora tengo un modelo, sino sería un clase normal sin superpoderes.
    protected $table = 'alumnos';
    protected $primaryKey = 'id';
    protected $returnType = 'object'; //porque me gusta
    //hemos de decir que campos son modificables.
    protected $allowedFields = ['NIA','nombre','apellido1','apellido2','nif','email'];
    protected $validationRules = [
        'NIA'       => 'required|numeric|min_length[8]|is_unique[alumnos.NIA]',
        'nombre'    => 'required',
        'apellido1' => 'trim',
        'email'     => 'valid_email',
        'nif'       => 'is_unique[alumnos.nif]',
    ];
    
    
}
